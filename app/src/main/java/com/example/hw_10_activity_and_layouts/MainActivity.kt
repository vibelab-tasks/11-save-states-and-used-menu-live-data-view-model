package com.example.hw_10_activity_and_layouts

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import kotlin.random.Random

const val LAST_NAME_KEY = "last_name"
const val FIRST_NAME_KEY = "first_name"
const val SECOND_NAME_KEY = "second_name"
const val AGE_KEY = "age"
const val HOBBY_KEY = "hobby"

class MainActivity : AppCompatActivity() {

    private lateinit var clMainLayout: ConstraintLayout

    private lateinit var etLastName: EditText
    private lateinit var etFirstName: EditText
    private lateinit var etSecondName: EditText
    private lateinit var etAge: EditText
    private lateinit var etHobby: EditText

    private lateinit var btnInfoActivity: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        clMainLayout = findViewById(R.id.clMainLayout)

        etLastName = findViewById(R.id.etLastName)
        etFirstName = findViewById(R.id.etFirstName)
        etSecondName = findViewById(R.id.etSecondName)
        etAge = findViewById(R.id.etAge)
        etHobby = findViewById(R.id.etHobby)
        btnInfoActivity = findViewById(R.id.btnInfoActivity)

        btnInfoActivity.setOnClickListener {
            val intent = Intent(this, InfoActivity::class.java)

            val textFields = keysToTextViews()
            textFields.keys.forEach {
                intent.putExtra(it, textFields[it]?.text.toString())
            }

            startActivity(intent)
        }

        putSavedData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.miSaveToPrefs -> {
                saveData()
                Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show()
            }

            R.id.miRandomBackground -> {
                val backgroundImages = listOf(
                    R.drawable.bg_1, R.drawable.bg_3, R.drawable.bg_3,
                    R.drawable.bg_4, R.drawable.bg_5)
                val randIndex = Random.nextInt(0, backgroundImages.size - 1)

                clMainLayout.setBackgroundResource(backgroundImages[randIndex])
                Toast.makeText(this, "Image changed", Toast.LENGTH_SHORT).show()
            }

            R.id.miOpenLayout -> {
                val intent = Intent(this, UselessActivity::class.java)
                startActivity(intent)
            }
        }
        return true
    }

    private fun saveData() {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)

        val textFields = keysToTextViews()

        sharedPreferences.edit().apply {
            textFields.keys.forEach {
                putString(it, textFields[it]?.text.toString())
            }
        }.apply()
    }

    private fun putSavedData() {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)

        val textFields = keysToTextViews()

        textFields.keys.forEach{
            textFields[it]?.text = sharedPreferences.getString(it, "")
        }
    }

    private fun keysToTextViews(): MutableMap<String, TextView> = mutableMapOf (
        FIRST_NAME_KEY to etLastName,
        LAST_NAME_KEY to etFirstName,
        SECOND_NAME_KEY to etSecondName,
        AGE_KEY to etAge,
        HOBBY_KEY to etHobby
        )
}