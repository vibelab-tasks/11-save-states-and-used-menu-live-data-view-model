package com.example.hw_10_activity_and_layouts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.MultiAutoCompleteTextView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

class UselessActivity : AppCompatActivity() {

    private lateinit var viewModel: UselessViewModel

    private lateinit var etUselessText: EditText
    private lateinit var btnSaveText: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_useless)

        viewModel = ViewModelProvider(this).get(UselessViewModel::class.java)

        etUselessText = findViewById(R.id.etUselessText)
        btnSaveText = findViewById(R.id.btnSaveText)


        viewModel.currentText.observe(this, Observer {
            etUselessText.setText(it.toString())
        })

        btnSaveText.setOnClickListener {
            viewModel.text = etUselessText.text.toString()
            viewModel.currentText.value = viewModel.text
        }

    }
}