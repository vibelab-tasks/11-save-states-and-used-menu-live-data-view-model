package com.example.hw_10_activity_and_layouts

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class UselessViewModel : ViewModel() {
    var text = ""

    val currentText: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
}